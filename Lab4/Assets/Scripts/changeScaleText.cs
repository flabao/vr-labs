﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeScaleText : MonoBehaviour {

    GameObject sizeReference;
    Sizeable sizeable;

    // Use this for initialization
    void Start () {
        sizeReference = GameObject.Find("Sizeable");
        sizeable = sizeReference.GetComponent<Sizeable>();
    }
	
	// Update is called once per frame
	void Update () {
        TextMesh t = (TextMesh)this.gameObject.GetComponent(typeof(TextMesh));
        t.text = sizeable.ValueX.ToString() + 
            " \n" + sizeable.ValueY.ToString() + 
            " \n" + sizeable.ValueZ.ToString();
    }
}
