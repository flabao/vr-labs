﻿
//
//  This file is an example implementation of a physical control.
//  Be sure to check Interactable.cs to get more information.
//

using System;
using UnityEngine;
using UnityEngine.Events;

// Even though we inherit from Interactable, Interactable inherits from MonoBehaviour,
// So we still have access to all of its fields and methods (e.g. this.transform, this.Invoke(), etc.)
public class Sizeable : Interactable {

    public float ValueX;                 // Between -0.5 and 0.5
    public float ValueY;
    public float ValueZ;

    public float xDistance;
    public float yDistance;
    public float zDistance;

    [Header("Configuration")]
    [Range(0.01f, 1)]                   // This attribute makes the field underneath into a slider in the editor.
    public float pullDelay = .05f;      // The responsiveness with which the lever matches the controller's position.
    public float Accuracy = 0.01f;
    public float BreakDistance = .1f;   // Distance in worldspace units at which the user is forced to let go of the lever.
    [Header("Spring")]
    public bool isSpring = false;      // If set, the handle returns to a resting position
    public float SpringDelay = .05f;    // The responsiveness with which the lever returns to its restingValue.
    public float RestingValue = .1f;
    [Header("Haptics")]
    public float dragHaptics = .5f;     // Multiplier for the haptic feedback when dragging. 
    public float hoverHaptics = 1f;     // Multiplier for the haptic feedback when hovering. 

    // See end of file (below) for a thorough explination of this field.
    public FloatEvent OnValueChanged;

    // We assume that the lever is 1 unit long.
    const float rodLocalRange = 5f;

    // References to child objects.
    private Transform handle;
    private Transform handleBar;

    // We also have access (from the base class) to:
    // bool Stealable;
    // WandController attachedController;

    void Start() {
        handle = transform.FindChild("Handle");
        handleBar = handle.FindChild("HandleBar");
        ValueX = handle.localScale.x;
        ValueY = handle.localScale.y;
        ValueZ = handle.localScale.z;
        Debug.Log(handleBar);
    }

    // Update is called once per frame
    void Update() {

        float newX = ValueX;
        float newY = ValueY;
        float newZ = ValueZ;

        if (attachedController != null) {
            Vector3 controllerPos = attachedController.transform.position;

            // check to see if controller is too far from lever handle.
            float distanceToHandleBar = Vector3.Distance(controllerPos, handleBar.position);
            if (distanceToHandleBar > BreakDistance) {
                // Send a click to the controller if we disconnect.
                attachedController.input.TriggerHapticPulse(2999);
                DetachController();
                return;
            }

            // Find the controller coordinate in local space (position, orientation and scale independent);
            Vector3 localPos = this.transform.InverseTransformPoint(controllerPos);
            // Take movement from the object's 3 axes
            float TargetX = localPos.x;
            float TargetY = localPos.y;
            float TargetZ = localPos.z;

            // Limit the Lever's movent to its effective range.
            TargetX = Mathf.Clamp(TargetX, 0.1f, rodLocalRange);
            TargetY = Mathf.Clamp(TargetY, 0.1f, rodLocalRange);
            TargetZ = Mathf.Clamp(TargetZ, 0.1f, rodLocalRange);

            // Find the difference of the coordinates. We use this to find out when to stop
            // animating the lever, as well as to provide haptic feedback to simulate resistance
            
            xDistance = Mathf.Abs(TargetX - ValueX);
            yDistance = Mathf.Abs(TargetY - ValueY);
            zDistance = Mathf.Abs(TargetZ - ValueZ);

            if (xDistance > Accuracy) {

                float hapticStrengthX = 2999 * (xDistance + .1f) * dragHaptics;
                
                hapticStrengthX = Mathf.Clamp(hapticStrengthX, 0, 2999);
                
                attachedController.input.TriggerHapticPulse((ushort)hapticStrengthX);

                //This approximates our target in a smooth fashion.
                newX = Mathf.Lerp(ValueX, TargetX, (1 / pullDelay) * Time.deltaTime);
            }

            if(yDistance > Accuracy) {
                float hapticStrengthY = 2999 * (yDistance + .1f) * dragHaptics;
                hapticStrengthY = Mathf.Clamp(hapticStrengthY, 0, 2999);

                attachedController.input.TriggerHapticPulse((ushort)hapticStrengthY);
                newY = Mathf.Lerp(ValueY, TargetY, (1 / pullDelay) * Time.deltaTime);
            }

            if (zDistance > Accuracy) {
                float hapticStrengthZ = 2999 * (zDistance + .1f) * dragHaptics;
                hapticStrengthZ = Mathf.Clamp(hapticStrengthZ, 0, 2999);

                attachedController.input.TriggerHapticPulse((ushort)hapticStrengthZ);
                newZ = Mathf.Lerp(ValueZ, TargetZ, (1 / pullDelay) * Time.deltaTime);
            }
        }
        else if (isSpring) {
            // Reset to resting position when detached.
            newX = Mathf.Lerp(ValueX, RestingValue, (1 / SpringDelay) * Time.deltaTime);
            newY = Mathf.Lerp(ValueY, RestingValue, (1 / SpringDelay) * Time.deltaTime);
            newZ = Mathf.Lerp(ValueZ, RestingValue, (1 / SpringDelay) * Time.deltaTime);
        }
        // Call the method that is linked in the editor.
        if (newX != ValueX) OnValueChanged.Invoke(ValueX);
        if (newY != ValueY) OnValueChanged.Invoke(ValueY);
        if (newZ != ValueZ) OnValueChanged.Invoke(ValueZ);

        ValueX = newX;
        ValueY = newY;
        ValueZ = newZ;
        
        //ValueX = xDistance;
        //ValueY = yDistance;
        //ValueZ = zDistance;

        // We need to copy the Vector because it is a property struct.
        Vector3 oldPos = handle.localPosition;
        Vector3 oldScale = handle.localScale;

        oldScale.x = ValueX;
        oldScale.y = ValueY;
        oldScale.z = ValueZ;
        //tried to do some translation to offset the scale, 
        //but it looks like this is not the right place to put it
        handle.localPosition = oldPos - oldPos;
        handle.localScale = oldScale;
        handle.localPosition = oldPos;
    }

    public override void OnHoverEnter(WandController ctrl) {
        ctrl.input.TriggerHapticPulse((ushort)(500 * hoverHaptics));
        handle.GetComponent<MeshRenderer>();
    }
}

// A UnityEvent is a field that allows you to setup a callback using the inspector.
// However, if you want your event to take parameters, you have to make a subclass
// This is simply because the inspector doesn't support generics.
//[Serializable] public class FloatEvent : UnityEvent<float> { }
