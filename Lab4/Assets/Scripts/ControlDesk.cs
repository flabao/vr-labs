﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDesk : MonoBehaviour
{
    GameObject text;
    void Start() {
        text = GameObject.Find("PositionText");
    }
    
    public Transform SelectedObject;
    public void SetX(float x)
    {
        Vector3 pos = SelectedObject.localPosition;
        pos.x = x;
        SelectedObject.localPosition = pos;
        
    }
    public void SetY(float y)
    {
        Vector3 pos = SelectedObject.localPosition;
        pos.y = y;
        SelectedObject.localPosition = pos;
    }
    public void SetZ(float z)
    {
        Vector3 pos = SelectedObject.localPosition;
        pos.z = z;
        SelectedObject.localPosition = pos;
    }

    public void SetScaleX(float x) {
        Vector3 scale = SelectedObject.localScale;
        scale.x = x;
        SelectedObject.localScale = scale;
    }

    public void SetScaleY(float y) {
        Vector3 scale = SelectedObject.localScale;
        scale.y = y;
        SelectedObject.localScale = scale;
    }

    public void SetScaleZ(float z) {
        Vector3 scale = SelectedObject.localScale;
        scale.z = z;
        SelectedObject.localScale = scale;
    }

}
