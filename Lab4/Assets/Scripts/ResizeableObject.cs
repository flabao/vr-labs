﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ResizeableObject : Interactable {
    private Transform sizeable;
    private Transform ball;

    public float scaleValX;
    public float scaleValY;
    public float scaleValZ;
    
    [Header("Configuration")]
    [Range(0.01f, 1)]
    public float pullDelay = 0.05f;
    public float Accuracy = 0.01f;
    public float BreakDistance = .1f;

    [Header("Spring")]
    public bool isSpring = false;
    public float SpringDelay = 0.05f;
    public float RestingValue = 0.1f;

    [Header("Haptics")]
    public float stretchHaptics=0.5f;
    public float hoverHaptics = 1f;

    // A UnityEvent is a field that allows you to setup a callback using the inspector.
    // However, if you want your event to take parameters, you have to make a subclass
    // This is simply because the inspector doesn't support generics.
    //[Serializable] public class FloatEvent : UnityEvent<float> { }
    public FloatEvent OnValueChanged;

    //how big the ball is allowed to get
    const float objLocalRange = 2f;
   
    
    void Start () {
        //sizeable = transform.Find("Sizeable");
        //ball = sizeable.FindChild("Planet");
        ball = transform.FindChild("Planet");
        scaleValX = ball.localScale.x;
        scaleValY = ball.localScale.y;
        scaleValZ = ball.localScale.z;
        Debug.Log(ball);
	}
	
	// Update is called once per frame
	void Update () {
        float newValX = scaleValX;
        float newValY = scaleValY;
        float newValZ = scaleValZ;

        if (attachedController != null) {
            Vector3 controllerPos = attachedController.transform.position;

            float distanceToBall = Vector3.Distance(controllerPos, ball.position);
            if (distanceToBall > BreakDistance) {
                attachedController.input.TriggerHapticPulse(2999);
                DetachController();
                return;
            }

            Vector3 localPos = this.transform.InverseTransformPoint(controllerPos);

            //take movement along the different axes
            float TargetX = localPos.x;
            float TargetY = localPos.y;
            float TargetZ = localPos.z;

            //limit lever scale size
            TargetZ = Mathf.Clamp(TargetZ, 0.1f,objLocalRange);
            /*
            //find diff of the coordinates
            float xDistance=Mathf.Abs(TargetZ,0)

            if (xDistance > Accuracy) {
                
            }
            */
        }
        else if (isSpring) {
            //reset scale
            newValX = Mathf.Lerp(scaleValX, RestingValue, (1 / SpringDelay) * Time.deltaTime);
            newValY = Mathf.Lerp(scaleValY, RestingValue, (1 / SpringDelay) * Time.deltaTime);
            newValZ = Mathf.Lerp(scaleValZ, RestingValue, (1 / SpringDelay) * Time.deltaTime);

        }
        //call method linked in the editor
        if((newValX!=scaleValX)|| newValX != scaleValX || newValX != scaleValX) {
            OnValueChanged.Invoke(newValX);
            //OnValueChanged.Invoke(newValY);
            //OnValueChanged.Invoke(newValZ);
        }
        scaleValX = newValX;
        scaleValY = newValY;
        scaleValZ = newValZ;

        Vector3 oldScale = ball.localScale;
        oldScale.x = scaleValX;
        oldScale.y = scaleValY;
        oldScale.z = scaleValZ;
        ball.localScale = oldScale;
    }

    public override void OnHoverEnter(WandController ctrl) {
        ctrl.input.TriggerHapticPulse((ushort)(500 * hoverHaptics));
        ball.GetComponent<MeshRenderer>();
    }
}
