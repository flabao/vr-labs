﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : MonoBehaviour {

    //camera head
    public GameObject camhead;
    public Vector3 difference;
    //teleport indicator
    public Rigidbody blueBall;

    //for controller coordinates
    public Vector3 contEndpos;
    public Transform contEnd;
    public Vector3 contEndforward;
    public SteamVR_Controller.Device contDevice;

    //for raycast shooting and ray viewer
    public float fireRate = .25f;
    public float weaponRange = 25f;
    public Transform gunEnd;

    RaycastHit hit;

    private Camera fpsCam;
    private WaitForSeconds shotDuration = new WaitForSeconds(.1f);
    private LineRenderer laserLine;
    private float nextFire;

    //for grabbing stuff
    private GameObject colliding;
    private GameObject holding;

	void Start () {
        var trackedObj = this.GetComponent<SteamVR_TrackedObject>();
        contDevice = SteamVR_Controller.Input((int)trackedObj.index);
        laserLine = GetComponent<LineRenderer>();
    }
	
	void Update () {
        contEndpos = this.transform.position;
        contEnd = this.GetComponent<Transform>();
        contEndforward = this.transform.forward;

        //When touchpad is pressed down, then shoot laser
        if (contDevice.GetTouch(SteamVR_Controller.ButtonMask.Touchpad) || 
            contDevice.GetPress(1ul<<8)/*x button*/) {
            nextFire = Time.time + fireRate;
            laserLine.enabled = true;
            //StartCoroutine(ShotEffect());

            Vector3 rayOrigin = contEndpos;

            laserLine.SetPosition(0, contEndpos);

            if(Physics.Raycast(rayOrigin,contEndforward,out hit, weaponRange)) {
                
                    laserLine.SetPosition(1, hit.point);
                if (hit.transform.tag=="Platform") {
                    makeBallAppear(blueBall, hit.point);
                }
            }
            else {
                laserLine.SetPosition(1, rayOrigin + contEndforward * weaponRange);
            }
            
        }
        else {
            laserLine.enabled = false;
        }

        if(contDevice.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad)) {

            if (hit.transform.tag=="Platform") {

                
                difference = transform.parent.position - camhead.transform.position;
                difference.y = 0;

                //SteamVR_Fade.View(Color.black, 0.1f);
                //FadeToBlack();

                SteamVR_Fade.View(Color.black, 0);
                SteamVR_Fade.View(Color.clear, 1);
                transform.parent.position = hit.point + difference;
                //SteamVR_Fade.View(Color.clear, 0.1f);

            }
        }
        /*
        //when trigger is held down, grab object
        if (contDevice.GetPress(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger)) {
            if (colliding) {
                GrabObject();
            }
            if (holding) {
                ReleaseObject();
            }
        }*/
        
        
        if (contDevice.GetHairTriggerDown()) {
            if (colliding) {
                GrabObject();
            }
        }
        
        if (contDevice.GetHairTriggerUp()) {
            if (holding) {
                ReleaseObject();
            }
        }
        
        //viewing ray
        Vector3 lineOrigin = contEndpos;
        Debug.DrawRay(lineOrigin, contEndforward * weaponRange, Color.green);
	}

    private IEnumerator ShotEffect() {
        laserLine.enabled = true;
        yield return shotDuration;
        laserLine.enabled = false;
    }

    private IEnumerator FadeToBlack() {
        //SteamVR_Fade.View(Color.black, .1f);
        yield return new WaitForSeconds(1f);
        SteamVR_Fade.View(Color.clear, .1f);
    }

    public Rigidbody makeBallAppear(Rigidbody ball,Vector3 hitpoint) {
        Rigidbody ballInstance;
        ballInstance = Instantiate(ball, hitpoint, contEnd.rotation) as Rigidbody;
        return ballInstance;
    }

    private void SetCollidingObject(Collider col) {
        if(colliding || !col.GetComponent<Rigidbody>()) {
            return;
        }
        colliding = col.gameObject;
    }

    public void OnTriggerEnter(Collider other) {SetCollidingObject(other);}
    public void OnTriggerStay(Collider other) {SetCollidingObject(other);}
    public void OnTriggerExit(Collider other) {
        if (!colliding) {
            return;
        }

        colliding = null;
    }

    private void GrabObject() {
        holding = colliding;
        colliding = null;

        var joint = AddFixedJoint();
        joint.connectedBody = holding.GetComponent<Rigidbody>();
    }

    private FixedJoint AddFixedJoint() {
        FixedJoint fjoint = gameObject.AddComponent<FixedJoint>();
        fjoint.breakForce = 2000;
        fjoint.breakTorque = 2000;
        return fjoint;
    }

    private void ReleaseObject() {
        if (GetComponent<FixedJoint>()) {
            GetComponent<FixedJoint>().connectedBody = null;
            Destroy(GetComponent<FixedJoint>());
            
            holding.GetComponent<Rigidbody>().velocity = contDevice.velocity;
            holding.GetComponent<Rigidbody>().angularVelocity = contDevice.angularVelocity;
        }
        holding = null;
    }
}
