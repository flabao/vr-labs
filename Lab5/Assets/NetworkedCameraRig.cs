﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkedCameraRig : NetworkBehaviour {

    // See ColorSync.cs for more info on SyncVars
    [SyncVar(hook = "OnUserNameChange")]
    public string UserName;
    // Scale of the camerarig. This is preferable to editing the CameraRig prefab.
    public float scaleFactor;
    //Prefab refernces
    public GameObject CameraRigPrefab;
    // The sphere prefab needs to be registered in the networkManager.
    public GameObject spherePrefab;
    // The rock prefab.
    public GameObject rockPrefab;
    // Sparks Prefab
    public GameObject sparks;
    //Instance references.
    GameObject CameraRig;
    Transform NetHead;
    Transform TrackedHead;


    [SyncVar]
    public int health = 10;
    

    void Start () {

        NetHead = transform.Find("Head");
        
        // This line is essential:
        if (isLocalPlayer) {

            //From the login-screen.
            UserName = PlayerPrefs.GetString("PLAYER_USERNAME");
            CmdRegisterUserName(UserName);

            CameraRig = Instantiate(CameraRigPrefab);

            //Our starting position is dictated by the networkManager's StartPositions.
            //These can be placed in the scene (Empty objects) and given a NetworkStartPosition Component
            //They will be automatically registered.
            CameraRig.transform.position = transform.position;
            CameraRig.transform.rotation = transform.rotation;
            CameraRig.transform.localScale = Vector3.one * scaleFactor;
            
            // Add the NetworkInput component to the controller avatars. Remember that this only happens on a local player.
            var leftNetInput = transform.Find("LeftController").gameObject.AddComponent<NetworkInput>();
            var rightNetInput = transform.Find("RightController").gameObject.AddComponent<NetworkInput>();

            // Setting References.
            var controllerManager = CameraRig.GetComponent<SteamVR_ControllerManager>();
            leftNetInput.trackedObject = controllerManager.left.GetComponent<SteamVR_TrackedObject>();
            rightNetInput.trackedObject = controllerManager.right.GetComponent<SteamVR_TrackedObject>();
            leftNetInput.player = this;
            rightNetInput.player = this;
            var svrCamera = CameraRig.transform.GetComponentInChildren<SteamVR_Camera>();
            TrackedHead = svrCamera.transform;

            // Small hack to avoid seeing the username text (You could also put it above the player..)
            svrCamera.camera.nearClipPlane = 0.26f;
        }
        else {
            // When we start a client, It recieves the initial state of the syncvars,
            // but it doesn't call the hook methods. We need to do this ourselves.
            OnUserNameChange(UserName);
        }
    }

    
    [Command]
    internal void CmdCreateSphere(Vector3 pos, Quaternion rotation)
    {
        //Instantiate on the sever
        GameObject obj = Instantiate(spherePrefab, pos, rotation);
        Rigidbody rig = obj.GetComponent<Rigidbody>();

        rig.AddForce(rig.transform.forward * 1000);

        //Instantiate on the client
        NetworkServer.Spawn(obj);
    }
    [Command]
    internal void CmdCreateRock(int identity) {
        
        
        
        GameObject controller;
        if (identity == 1) {
            controller = CameraRig.GetComponent<SteamVR_ControllerManager>().right.GetComponent<SteamVR_TrackedObject>().gameObject;
        } else {
            controller = CameraRig.GetComponent<SteamVR_ControllerManager>().left.GetComponent<SteamVR_TrackedObject>().gameObject;
        }
        GameObject obj = Instantiate(rockPrefab, controller.transform.position + controller.transform.forward / 2, controller.transform.rotation);
        FixedJoint joint;
        if (controller.GetComponent<FixedJoint>()) {
            joint = controller.GetComponent<FixedJoint>();
        }
        else {
            joint = controller.AddComponent<FixedJoint>();
        }

        joint.connectedBody = obj.GetComponent<Rigidbody>();
        joint.breakForce = 2000;
        joint.breakTorque = 2000;
        NetworkServer.Spawn(obj);
        
    }
    [Command]
    internal void CmdReleaseSparks(Vector3 pos, Quaternion rotation) {
        GameObject theSpawn = Instantiate(sparks, pos, rotation);
        NetworkServer.Spawn(theSpawn);

    }
    [Command]
    internal void CmdReleaseRock(int identity) {
        GameObject controller;
        if (identity == 1) {
            controller = CameraRig.GetComponent<SteamVR_ControllerManager>().right.GetComponent<SteamVR_TrackedObject>().gameObject;
        }
        else {
            controller = CameraRig.GetComponent<SteamVR_ControllerManager>().left.GetComponent<SteamVR_TrackedObject>().gameObject;
        }
        var theJoint = controller.GetComponent<FixedJoint>();
        if (theJoint && theJoint.connectedBody && theJoint.connectedBody.tag == "projectile") {
            var theObj = theJoint.connectedBody;
            theJoint.connectedBody = null;
            Destroy(theJoint);
            theObj.GetComponent<Rigidbody>().velocity = (theObj.transform.position - theObj.GetComponent<KillMe>().getPrevPos()) * 30;
            theObj.GetComponent<Rigidbody>().angularVelocity = (theObj.transform.position - theObj.GetComponent<KillMe>().getPrevPos()) * 30;

        }
    }

    void Update () {
        // We do the head Avatar tracking here.
        if (isLocalPlayer) {
            NetHead.position = TrackedHead.position;
            NetHead.rotation = TrackedHead.rotation;
        }
    }

    //Syncvar Callback for user's name
    void OnUserNameChange(string newName)
    {
        var model = transform.FindChild("HeadModel");
        var tm = model.GetComponentInChildren<Text>();
        tm.text = newName;
        UserName = newName;
    }

    // Sends a message from the local player so that the server can then propagate to other clients.
    [Command]
    void CmdRegisterUserName(string username) {
        this.UserName = username;
    }

    public GameObject getCameraRig() {
        return CameraRig;
    }


}
