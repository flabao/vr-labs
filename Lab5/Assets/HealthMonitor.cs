﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HealthMonitor : NetworkBehaviour {

    public const int maxHealth = 10;
    // SyncVars have to be updated where the authority lies. In this case, the authority of ball lies in the server.
    [SyncVar]
    public int health = maxHealth;

    // When you call a hook from a syncVar, instead of updating the value in all the other clients, the game calls a method
    //[SyncVar(hook = "ApplyHealth")]
    //public Color myHealth;



    private void OnCollisionEnter(Collision collision) {
        //Disregard collisions on the clients.
        if (collision.gameObject.tag=="projectile") {
            
            health--;
            RpcYell();
            print("HEALTH GOES DOWN");
        }
    }
    // Rpcs need to have their names begin with Rpc... Question: Does this execute on the client, the server, or both?
    [ClientRpc]
    void RpcYell() {
        print("Health: " + health);
    }
    //The parameter is the syncvar's new value.
    public void TakeDamage(int amount) {
        /*
        if (!isServer) {
            return;
        }*/

        health -= amount;
        if (health <= 0) {
            //this.GetComponent<GameObject>().renderer.material.
            health = 0;
            print("YOU ARE DEAD");
        }
        print("My health is: " + health);
    }
}
