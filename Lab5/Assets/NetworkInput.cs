﻿using UnityEngine;
using UnityEngine.Networking;

//This object is added to the controller network avatars when the program is running as a client.
[RequireComponent(typeof(Rigidbody), typeof(FixedJoint))]
public class NetworkInput : MonoBehaviour {
    
    public SteamVR_TrackedObject trackedObject;
    public NetworkedCameraRig player;
    GameObject model;
    
    private void Start()
    {
        // Consider why our models are neither meshRenderers on the network avatars, nor child objects of them.
        // How come we have to use joints? 

        model = transform.parent.FindChild(this.name + "Model").gameObject;
    }

    void Update () {
        if (trackedObject)
        {
            // disable hands if not tracked.
            model.SetActive(trackedObject.isValid);
            if (trackedObject.isValid) {
                // have the net avatars track the steamvr tracked-objects
                transform.position = trackedObject.transform.position;
                transform.rotation = trackedObject.transform.rotation;

                // Send input events to the NetworkedCameraRig instance. (This is our local player)
                var input = SteamVR_Controller.Input((int)trackedObject.index);
                if (input.GetPressDown(Valve.VR.EVRButtonId.k_EButton_ApplicationMenu)) {
                    // Why does this method have to live in the NetworkedCameraRig class?
                    player.CmdCreateSphere(transform.position + transform.forward / 3, transform.rotation);
                    //print(transform.rotation);
                }

                if (player.getCameraRig().GetComponent<SteamVR_ControllerManager>().right.GetComponent<SteamVR_TrackedObject>()) {
                    int ident;
                    if (trackedObject == player.getCameraRig().GetComponent<SteamVR_ControllerManager>().right.GetComponent<SteamVR_TrackedObject>()) {
                        ident = 1;
                    }
                    else {
                        ident = 2;
                    }

                    if (input.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger)) {
                        player.CmdCreateRock(ident);
                    }
                    else if (input.GetPressUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger)) {
                        player.CmdReleaseRock(ident);
                    }
                }
                if (input.GetPressDown(Valve.VR.EVRButtonId.k_EButton_Grip)) {
                    player.CmdReleaseSparks(transform.position, transform.rotation);
                }
            } 
        }
    }

}
