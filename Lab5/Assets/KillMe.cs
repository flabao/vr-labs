﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMe : MonoBehaviour {

    private int count;
    public int lifeTime = 200;
    private Vector3 previousPos;
    private Vector3 curPos;

	// Use this for initialization
	void Start () {
        count = 0;
        previousPos = transform.position;
        curPos = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        if (count >= lifeTime) {
            Destroy(gameObject);
        }
        count++;
        previousPos = curPos;
        curPos = transform.position;

	}

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "PlayerHead") {
            Destroy(gameObject);
            collision.gameObject.GetComponent<HealthMonitor>().TakeDamage(1);
        }
        if (collision.gameObject.tag == "projectile") {
            Destroy(gameObject);
        }
    }

    public Vector3 getPrevPos() {
        return previousPos;
    }
}
